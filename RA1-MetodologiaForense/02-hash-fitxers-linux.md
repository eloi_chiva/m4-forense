# Repte:  Crear hash en sistemes *nix

## Referències

La comanda "stat fitxer" ens dona informació de marques de temps de fitxers *nix. Són 4.

Time fields meaning:

ctime: file change time.
atime: file access time.
mtime: file modification time.
crtime: file creation time.

## Objectius

- Fer operacions de hash en arxius i comprovar com afecten els canvis als hashs de fitxers.

## Exercici

- Crea o selecciona un fitxer de text que contingui informació.
- Calcula i registra el hash del fitxer.
- Fes els següents canvis i comprova si canvia el valor de hash:
  - Canvia un caràcter
  - Canvia el nom del fitxer amb "mv fitxer.txt noufitxer.txt"
  - Canvia els drets d'accés (amb chmod), per exemple "chmod g+w fitxer.txt"
  - Canvia la data de modificació amb touch "29-02-2004  16:21:42" fitxer.txt
  - Crea un enllaç simbòlic al fitxer, i calcula el hash de l'enllaç simbòlic, per exemple "ln –s fitxer.txt"

## Preguntes

Quins canvis fan que el hash es modifiqui? Quina explicació li dones?
